package customerFrontend.control;

import contract.CustomerContract;
import dto.*;
import dummy.control.CustomerDummyManager;
import eto.*;
import java.io.Serializable;
import java.util.*;
import java.util.logging.*;
import javax.faces.bean.*;

@ManagedBean(name = "reservationBean")
@SessionScoped
public class ReservationBean implements Serializable {

    private final CustomerContract manager;
    private TrafficDetail trafficDetail;
    private List<TrafficSummary> trafficInformation;
    private TrafficSummary selectedSchedule;
    private AccountSummary reserver;
    private PassengerDTO passenger1;
    private PassengerDTO passenger2;
    private CarDTO vehicle1;
    private CarDTO vehicle2;
    private TravelSummary travelSummary;
    private ReservationSummary reservationSummary;

    /**
     * Creates a new instance of ReservationBean
     */
    public ReservationBean() {
        long id = (int) (Math.random() * 100000);
        trafficDetail = new TrafficDetail();
        manager = new CustomerDummyManager();
        reserver = new AccountSummary(id);
        passenger1 = new PassengerDTO(id);
        passenger2 = new PassengerDTO(id);
        vehicle1 = new CarDTO();
        vehicle2 = new CarDTO();
    }
    
    //business logic////////////////////////////////////////////////////////////

    public void processTrafficInformation() {
        try {
            trafficInformation = (List<TrafficSummary>) manager.getTrafficInformation(trafficDetail);
        } catch (InvalidRouteException | NoFerriesFoundException ex) {
            Logger.getLogger(ReservationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void processReservationInfo() {
        long id = (int) (Math.random() * 100000);
        Collection<PassengerDTO> passengerDTOs = new ArrayList<>();
        Collection<AbstractVehicle> vehicles = new ArrayList<>();
        passengerDTOs.add(passenger1);
        passengerDTOs.add(passenger2);
        vehicles.add(vehicle1);
        vehicles.add(vehicle2);
        TrafficSummary trafficSummary = new TrafficSummary(id, selectedSchedule.getFerry(), selectedSchedule.getDeparturePort(), selectedSchedule.getDestinationPort(), selectedSchedule.getDepartureTime(), selectedSchedule.getArrivalTime(), selectedSchedule.getPrice());
        TravelDetail travelDetail = new TravelDetail(id, reserver, passengerDTOs, vehicles, trafficSummary);
        try {
            travelSummary = manager.getTravelSummary(travelDetail);
        } catch (NoScheduleException ex) {
            Logger.getLogger(ReservationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void processReservation() {
        long id = (int) (Math.random() * 100000);
        String reservationSerialNumber = Long.toString(id);
        ReservationDetail reservationDetail = new ReservationDetail(travelSummary.getReserver(), travelSummary.getPassengers(), travelSummary.getVehicles(), selectedSchedule.getDeparturePort(), selectedSchedule.getDestinationPort(), selectedSchedule.getDepartureTime(), selectedSchedule.getArrivalTime(), travelSummary.getTotalPrice(), reservationSerialNumber);
        try {
            reservationSummary = manager.makeReservation(reservationDetail);
        } catch (NoSuchReservationException ex) {
            Logger.getLogger(ReservationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //JSF navigation////////////////////////////////////////////////////////////
    
    public String trafficInfo() {
        return "/faces/trafficInfo.xhtml";
    }
    
    public String reservationInfo() {
        return "/faces/reservationInfo.xhtml";
    }
    
    public String makeReservation() {
        return "/faces/makeReservation.xhtml";
    }
    
    public String reservationReceipt() {
        return "/faces/reservationReceipt.xhtml";
    }
    
    public String index() {
        return "faces/index.xhtml";
    }
    
    //DTO getters & setters/////////////////////////////////////////////////////

    public TrafficDetail getTrafficDetail() {
        return trafficDetail;
    }

    public void setTrafficDetail(TrafficDetail trafficDetail) {
        this.trafficDetail = trafficDetail;
    }

    public List<TrafficSummary> getTrafficInformation() {
        return trafficInformation;
    }

    public void setTrafficInformation(List<TrafficSummary> trafficInformation) {
        this.trafficInformation = trafficInformation;
    }

    public TrafficSummary getSelectedSchedule() {
        return selectedSchedule;
    }

    public void setSelectedSchedule(TrafficSummary selectedSchedule) {
        this.selectedSchedule = selectedSchedule;
    }

    public AccountSummary getReserver() {
        return reserver;
    }

    public void setReserver(AccountSummary reserver) {
        this.reserver = reserver;
    }

    public PassengerDTO getPassenger1() {
        return passenger1;
    }

    public void setPassenger1(PassengerDTO passenger1) {
        this.passenger1 = passenger1;
    }

    public PassengerDTO getPassenger2() {
        return passenger2;
    }

    public void setPassenger2(PassengerDTO passenger2) {
        this.passenger2 = passenger2;
    }

    public CarDTO getVehicle1() {
        return vehicle1;
    }

    public void setVehicle1(CarDTO vehicle1) {
        this.vehicle1 = vehicle1;
    }

    public CarDTO getVehicle2() {
        return vehicle2;
    }

    public void setVehicle2(CarDTO vehicle2) {
        this.vehicle2 = vehicle2;
    }

    public TravelSummary getTravelSummary() {
        return travelSummary;
    }

    public void setTravelSummary(TravelSummary travelSummary) {
        this.travelSummary = travelSummary;
    }

    public ReservationSummary getReservationSummary() {
        return reservationSummary;
    }

    public void setReservationSummary(ReservationSummary reservationSummary) {
        this.reservationSummary = reservationSummary;
    }
}
