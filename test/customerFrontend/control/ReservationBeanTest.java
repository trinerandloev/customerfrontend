package customerFrontend.control;

import dto.*;
import java.util.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class ReservationBeanTest {

    public ReservationBeanTest() {}

    /**
     * Test of processTrafficInformation method, of class ReservationBean.
     */
    @Test
    public void testProcessTrafficInformation() {
        System.out.println("processTrafficInformation");
        TrafficDetail trafficDetail = new TrafficDetail();
        ReservationBean instance = new ReservationBean();
        trafficDetail.setDeparturePort("Havnso");
        trafficDetail.setDestinationPort("Sejero");
        trafficDetail.setDepartureTime(new Date(2015, 0, 17));
        instance.setTrafficDetail(trafficDetail);
        instance.processTrafficInformation();
        List<TrafficSummary> trafficSummarys = new ArrayList<>();
        trafficSummarys.add(new TrafficSummary());
        List<TrafficSummary> expResult = trafficSummarys;
        List<TrafficSummary> result = instance.getTrafficInformation();
        assertTrue(result.size() >= expResult.size());
    }

    /**
     * Test of processReservationInfo method, of class ReservationBean.
     */
    @Test
    public void testProcessReservationInfo() {
        System.out.println("processReservationInfo");
        ReservationBean instance = new ReservationBean();
        long id = (int) (Math.random() * 100000);
        TrafficSummary trafficSummary = new TrafficSummary(id, "asdnd8", "Havnso", "Nekselo", new Date(2015, 3, 27, 6, 0), new Date(2015, 3, 27, 6, 0), new PriceDTO(75.0, 0.0));
        instance.setSelectedSchedule(trafficSummary);
        instance.processReservationInfo();
        TravelSummary travelSummary = instance.getTravelSummary();
        double expResult = new PriceDTO(75.0, 0.0).getPersonPrice()*2;
        double result = travelSummary.getTotalPrice();
        assertTrue(expResult == result);
    }
}
